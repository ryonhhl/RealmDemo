//
//  CompanyCache.swift
//  BestBuy
//
//  Created by 衡成飞 on 10/19/16.
//  Copyright © 2016 qianwang. All rights reserved.
//

import UIKit
import RealmSwift

class CompanyCache: Object {
    dynamic var id:Int = 0
    dynamic var name:String?
    let news = List<NewsCache>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
