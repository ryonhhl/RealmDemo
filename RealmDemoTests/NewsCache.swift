//
//  NewsCache.swift
//  BestBuy
//
//  Created by 衡成飞 on 10/19/16.
//  Copyright © 2016 qianwang. All rights reserved.
//

import UIKit
import RealmSwift

class NewsCache: Object {
    dynamic var id:Int = 0
    dynamic var title:String?
    dynamic var pic:String?
    dynamic var isShowed:Int = 0
    let isDeleted = RealmOptional<Int>()
    
    dynamic var create = Date()
    
    //Inverse Relationships
    let owners = LinkingObjects(fromType: CompanyCache.self, property: "news")
    
    override static func primaryKey() -> String? {
        return "id"
    }

    
    open var selected:Bool?
    override static func ignoredProperties() -> [String] {
        return ["seleced"]
    }
}
