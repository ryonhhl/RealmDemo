//
//  RealmDemoTests.swift
//  RealmDemoTests
//
//  参考网站：https://realm.io/docs/swift/latest/
//
//  Created by 衡成飞 on 10/19/16.
//  Copyright © 2016 qianwang. All rights reserved.
//

import XCTest
import RealmSwift

@testable import RealmDemo

class RealmDemoTests: XCTestCase {
    
    fileprivate var realm:Realm!
    
    override func setUp() {
        super.setUp()
        
        print(Realm.Configuration.defaultConfiguration.fileURL!.path)
        
        let config = Realm.Configuration(
            schemaVersion: 0,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    
                }
            }
        )
        Realm.Configuration.defaultConfiguration = config
        realm = try! Realm()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     insert
     **/
    func testInsert() {
        self.measure {
            let comCache = CompanyCache()
            comCache.id = 0
            comCache.name = "钱旺集团"
            
            let newsCache = NewsCache()
            newsCache.id = 0
            newsCache.title = "王宝强"
            
            let newsCache1 = NewsCache()
            newsCache1.id = 1
            newsCache1.title = "马蓉"
            
            let news = List<NewsCache>()
            news.append(newsCache)
            news.append(newsCache1)
            
            //此方式适合表子段不常变化的
            let comCache1 = CompanyCache(value: ["id": 1,
                                                 "name": "藤榕",
                                                 "news": [["id":2,"title":"赵本山"],["id":3,"title":"宋小宝"]]])
            //此处可通过append,也可添加整个数组
            //另外注意，关联表之间，一定要建立关系后添加，不要单独添加，否则两个表之间没有任何关系
            comCache.news.append(newsCache)
            comCache.news.append(newsCache1)
            
            try! self.realm.write {
                self.realm.add(comCache, update: true)
                self.realm.add(comCache1, update: true)
            }
        }
    }
    
    
    /**
     select
     **/
    func testSelect() {
        self.measure {
            //查询公司id>=0的所有公司
            let allObjects = self.realm.objects(CompanyCache.self).filter("id >= %@",0).sorted(byProperty: "id", ascending: true)
            
            //查询新闻中包括赵本山的所有公司
            let allObjects1 = self.realm.objects(CompanyCache.self).filter{$0.news.contains{$0.title == "赵本山"}}
            
            //查询单条数据
            let allObjects2 = self.realm.objects(CompanyCache.self).filter{$0.id == 0}.first
            let allObjects3 = self.realm.object(ofType: CompanyCache.self, forPrimaryKey: 0)
            
            print(allObjects)
            print(allObjects1)
            print(allObjects2)
            print(allObjects3)
        }
    }
    
    
    /**
     update
     **/
    func testUpdate() {
        self.measure {
            let obj0 = self.realm.object(ofType: CompanyCache.self, forPrimaryKey: 0)
            
            try! self.realm.write {
                obj0?.name = "xx0"
            }
        }
    }
    
    /**
     delete
     **/
    func testDelete() {
        self.measure {
            //注意，删除关系表时，父表和子表要一起删除
            let obj0 = self.realm.object(ofType: CompanyCache.self, forPrimaryKey: 0)
            
            //删除单条或多条数据
            if obj0 != Optional.none {
                try! self.realm.write {
                    self.realm.delete(obj0!.news)
                    self.realm.delete(obj0!)
                }
            }
            
            //删除所有数据
            //self.realm.deleteAll()
        }
    }
    
}
